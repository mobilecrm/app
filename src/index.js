import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'react-notifications/lib/notifications.css';
import {App} from './App';
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {appReducer} from "./store/reducers/app";
import thunk from "redux-thunk";
import {Provider} from "react-redux";
import {listReducer} from "./store/reducers/list";

const store = createStore(combineReducers({
    app: appReducer,
    list: listReducer
}), compose(
    applyMiddleware(
        thunk
    )
))

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

