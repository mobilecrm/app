export const colors = {
    default: {
        normal: "#868d95",
        hover: "#5b6573",
        active: "#3b506e",
        text: "#ffffff"
    },

    success: {
        normal: "#bbed21",
        hover: "#d2f95f",
        active: "#b2e232",
        text: "#535c69"
    },

    danger: {
        normal: "#f1361a",
        hover: "#cc1c00",
        active: "#d24430",
        text: "#ffffff"
    },

    primary: {
        normal: "#3bc8f5",
        hover: "#3eddff",
        active: "#12b1e3",
        text: "#ffffff"
    },

    link: {
        normal: "transparent",
        hover: "transparent",
        active: "transparent",
        text: "#535c69"
    }
}