import styled from "styled-components";
import {ControllWrapper} from "../ControllWrapper";
import {colors} from "./colors";
import React from "react";

const Component = styled.button`
  border-radius: ${props=>props.rounded?48:2}px;
  border: none;
  background: ${props=>props.color?colors[props.color].normal:colors.default.normal};
  color: ${props=>props.color?colors[props.color].text:colors.default.text};
  font-size: 16px;
  line-height: 22px;
  padding: 7px 0px;
  
  :hover{
    background: ${props=>props.color?colors[props.color].hover:colors.default.hover};
  }
  
  :active{
    background: ${props=>props.color?colors[props.color].active:colors.default.active};
  }
  
  
`

export const Button = props => <ControllWrapper><Component {...props}>{props.children}</Component></ControllWrapper>