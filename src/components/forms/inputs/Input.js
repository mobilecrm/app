import styled from "styled-components";
import {InputWrapper} from "./InputWrapper";
import React from "react";

const Component = styled.input`
  margin: 10px 0;
  padding: 7px 15px;
  border: 1px solid #B4B4B4;
  border-radius: 3px;
  background: #fff;
  color: #535c69;
  font-size: 16px;
  
  :active, :focus{
    border: 1px solid #787878;
  }
`
export const Input = props => <InputWrapper label={props.label}><Component {...props}/></InputWrapper>

