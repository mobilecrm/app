import styled from "styled-components";
import {ControllWrapper} from "../ControllWrapper";

export const InputWrapper = styled(ControllWrapper)`
  ${props=>props.label?`::before{
    content:"${props.label}";
    color: #a3a3a3;
    font-size: 14px;
    padding-bottom: 3px;
  }`:''}
`