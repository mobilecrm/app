import styled from "styled-components";

export const ControllWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: stretch;
  margin: 5px 0;
`
