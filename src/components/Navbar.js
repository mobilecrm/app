import React from 'react'
import {Row, Col} from "react-grid-system";
import {IoMenu} from "react-icons/io5";
import styled from "styled-components";
import {Icon} from "./miscellaneous/Icon";
import {useDispatch, useSelector} from "react-redux";
import {setNavMenuState} from "../store/actions";
import {Navmenu} from "./Navmenu";

const Component = styled(Row)`
   border-bottom: 1px solid #e8e8e8;
   padding: 15px 0;
   
   > div{
      text-align: center;
   }
`

export const Navbar = () => {
    const currState = useSelector(state=>state.app.isNavMenuOpened)
    const title = useSelector(state=>state.app.currentPageText)
    const dispatch = useDispatch()

    return <>
        <Component>
            <Col xs={2}/>
            <Col xs={8}>{title}</Col>
            <Col xs={2}>
                <Icon size={"l"} onClick={()=>dispatch(setNavMenuState(!currState))}>
                    <IoMenu />
                </Icon>
            </Col>
        </Component>
        <Navmenu />
    </>
}