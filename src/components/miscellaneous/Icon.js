import styled from "styled-components"

const sizes = {
    s: "15px",
    m: "20px",
    l: "25px"
}

export const Icon = styled.span`
  font-size: ${props=>props.size?sizes[props.size]:sizes.s};    
`