import React from 'react'
import styled from "styled-components";
import {useSelector} from "react-redux";
import {Link} from "react-router-dom";

const navList = [
    {
        title: 'Лиды',
        path: '/lead/list'
    },
    {
        title: 'Сделки',
        path: '/deal/list'
    },
    {
        title: 'Контакты',
        path: '/contact/list'
    },
    {
        title: 'Компании',
        path: '/company/list'
    },
]

const Component = styled.div`
    overflow: hidden;
    max-height: ${state=>state.opened?'500px':'0'};
    transition: max-height 1s linear;
    
    ul{
      list-style-type: none;
      padding: 0;
    }
    li{
      padding: 10px 0;
      border-bottom: 1px solid #e8e8e8;
    }
    
    a{
      text-decoration: none;
      color: black;
    }
`

export const Navmenu = () => {
    const opened = useSelector(state=>state.app.isNavMenuOpened)

    return <Component opened={opened}>
        <ul>
            {navList.map((v,k)=><li key={k}><Link to={v.path}>{v.title}</Link></li>)}
        </ul>
    </Component>
}