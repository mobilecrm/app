import {Row, Col} from "react-grid-system"
import styled from "styled-components";
import React from "react";
import {Icon} from "../miscellaneous/Icon";
import {MdChevronLeft, MdChevronRight} from "react-icons/all";
import {useDispatch, useSelector} from "react-redux";
import {listNextPage, listPrevPage} from "../../store/actions";

const Component = styled(Row)`
  padding: 10px 0;
  > div{
    text-align: center;
  }
`

export const Pagination = () => {
    const dispatch = useDispatch()
    const curPage = useSelector(state=>state.list.curPage)

    return <Component>
        <Col xs={3} />
        <Col xs={2}>
            {curPage===0?null:
                <Icon size={"m"}
                      onClick={()=>dispatch(listPrevPage())}><MdChevronLeft/></Icon>
            }
        </Col>
        <Col xs={2} >
            {curPage+1}
        </Col>
        <Col xs={2} >
            <Icon size={"m"}
                  onClick={()=>dispatch(listNextPage())}><MdChevronRight/></Icon>
        </Col>
        <Col xs={3} />
    </Component>
}