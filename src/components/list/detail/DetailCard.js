import {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {loadItem} from "../../../store/actions";

export const DetailCard = ({type, id}) => {
    const dispatch = useDispatch()
    const item = useSelector(state=>state.list.current)

    useEffect(()=>{
        dispatch(loadItem(type, id))
    },[dispatch, type, id])

    return <>
        {item.NAME}
    </>
}