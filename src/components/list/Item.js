import React from 'react'
import {Col} from "react-grid-system";
import styled from "styled-components";
import {Link} from "react-router-dom";

const Component = styled(Col)`
  padding: 15px;
  border-bottom: 1px solid #e8e8e8;
`

export const Item = ({fields, type}) => {
    let itemFields;

    switch(type){
        case 'lead':
        case 'deal':
        case 'company':
            itemFields = {
                title: fields.TITLE
            }
            break;
        case 'contact':
            itemFields = {
                title: `${fields.NAME} ${fields.LAST_NAME}`
            }
            break;
        default: itemFields = {}
    }

    return <Component xs={12}>
        <Link to={`/${type}/detail/${fields.ID}`}>{itemFields.title}</Link>
    </Component>
}