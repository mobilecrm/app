import React, {useEffect} from 'react'
import {useDispatch, useSelector} from "react-redux";
import {loadEntitiesList} from "../../store/actions";
import {Item} from "./Item";
import {Row} from "react-grid-system"

export const List = ({type}) => {
    const curPage = useSelector(state=>state.list.curPage)
    const elements = useSelector(state=>state.list.items)
    const dispatch = useDispatch()

    useEffect(()=>{
        dispatch(loadEntitiesList(type, curPage))
    }, [dispatch, type, curPage])

    return <Row>
        {elements.map(v=><Item key={v.ID} fields={v} type={type}/>)}
    </Row>
}