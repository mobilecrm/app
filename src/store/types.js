export const SET_API_TOKEN = "APP/API_TOKEN"
export const SET_NAVMENU_STATE = "APP/NAVMENU_STATE"
export const SET_APP_TITLE = "APP/NAVBAR_TITLE"

export const SET_LIST_ITEMS = "LIST/ITEMS"
export const SET_CURRENT_ITEM = "LIST/ITEM"
export const SET_PAGE = "LIST/PAGE"