import {SET_API_TOKEN, SET_APP_TITLE, SET_CURRENT_ITEM, SET_LIST_ITEMS, SET_NAVMENU_STATE, SET_PAGE} from "./types";
import axios from "axios";
import {NotificationManager} from "react-notifications";

export function setApiToken(apiToken){
    localStorage.setItem('apiToken', apiToken)
    return {
        type: SET_API_TOKEN,
        payload: apiToken
    }
}

export function setNavMenuState(state) {
    return {
        type: SET_NAVMENU_STATE,
        payload: state
    }
}

export function setPageTitle(title){
    return {
        type: SET_APP_TITLE,
        payload: title
    }
}

export function setListItems(items){
    return {
        type: SET_LIST_ITEMS,
        payload: items
    }
}

export function setCurrentItem(item){
    return {
        type: SET_CURRENT_ITEM,
        payload: item
    }
}

export function setCurrentPage(page) {
    return {
        type: SET_PAGE,
        payload: page
    }
}

export function auth(login, password){
    return async dispatch => {
        try {
            if(!login || !password)
                throw new Error('Empty param')
            const response = await axios.post('/services/auth/signIn', {
                login,
                password
            })
            const apiToken = response.data.result
            dispatch(setApiToken(apiToken))
        }catch(e){
            NotificationManager.error('Ошибка при авторизации')
        }
    }
}

export function loadEntitiesList(type, curPage){
    return async (dispatch, getState) => {
        try {
            const limit = 10;
            const offset = limit*curPage;

            if(!type)
                throw new Error('Empty param')
            const response = await axios.post(`/entities/${type}/list`,{
                api_token: getState().app.apiToken,
                query: {
                    limit,
                    offset
                },
            })
            dispatch(setListItems(response.data.result))
        }catch(e){
            dispatch(setListItems([]))
            if(e.response.status===404){
                NotificationManager.error('Ничего не найдено')
            }else{
                NotificationManager.error('Ошибка при загрузке')
            }
        }
    }
}

export function listNextPage(){
    return (dispatch, getState) => {
        const {curPage} = getState().list
        dispatch(setCurrentPage(curPage+1))
    }
}

export function listPrevPage(){
    return (dispatch, getState) => {
        const {curPage} = getState().list
        dispatch(setCurrentPage(curPage-1))
    }
}

export function loadItem(type, id) {
    return async dispatch => {
        try {
            if(!type)
                throw new Error('Empty param')
            const response = await axios.post(`/entities/${type}/get`,{
                id
            })
            dispatch(setCurrentItem(response.data.result))
        }catch(e){
            dispatch(setCurrentItem({}))
            if(e.response.status===404){
                NotificationManager.error('Элемент не найден')
            }else{
                NotificationManager.error('Ошибка при загрузке')
            }
        }
    }
}