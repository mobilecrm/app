import {SET_CURRENT_ITEM, SET_LIST_ITEMS, SET_PAGE} from "../types";

const initialState = {
    items: [],
    current: {},
    curPage: 0,
}

export const listReducer = (state=initialState, action) => {
    switch(action.type){
        case SET_LIST_ITEMS:
            return {
                ...state,
                items: action.payload
            }
        case SET_CURRENT_ITEM:
            return {
                ...state,
                current: action.payload
            }
        case SET_PAGE:
            return {
                ...state,
                curPage: action.payload
            }
        default: return state;
    }
}