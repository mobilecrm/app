import {SET_API_TOKEN, SET_APP_TITLE, SET_NAVMENU_STATE} from "../types";

const initialState = {
    currentPageText: "Главная",
    isNavMenuOpened: false,
    apiToken: localStorage.getItem('apiToken')
}

export const appReducer = (state = initialState, action) => {
    switch (action.type){
        case SET_API_TOKEN:
            return {
                ...state,
                apiToken: action.payload
            }
        case SET_NAVMENU_STATE:
            return {
                ...state,
                isNavMenuOpened: action.payload
            }
        case SET_APP_TITLE:
            return {
                ...state,
                currentPageText: action.payload
            }
        default: return state
    }
}