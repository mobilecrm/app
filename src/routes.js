import React from "react";
import {Switch, Route, Redirect} from 'react-router-dom'
import {SignIn} from "./pages/SignIn/SignIn";
import {Navbar} from "./components/Navbar";
import {EntitiesList} from "./pages/EntitiesList";
import {DetailCard} from "./components/list/detail/DetailCard";

export const Routes = isAuth => {
    if(isAuth){
        return <>
            <Navbar />
            <Switch>
                <Route path={'/home'} exact>
                    home
                </Route>
                <Route
                    path={'/:entity/list'}
                    render={({match})=><EntitiesList type={match.params.entity}/>} />
                <Route
                    path={'/:entity/detail/:id'}
                    render={({match})=><DetailCard type={match.params.entity}  id={match.params.id}/>} />
                <Redirect to={`/home`} />
            </Switch>
        </>
    }
    return <Switch>
        <Route path={'/signin'} exact>
            <SignIn />
        </Route>
        <Redirect to={`/signin`} />
    </Switch>
}