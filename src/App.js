import {Routes} from "./routes";
import {BrowserRouter as Router} from 'react-router-dom'
import {setConfiguration, Container} from 'react-grid-system'
import React from "react";
import {NotificationContainer} from 'react-notifications';
import {useSelector} from "react-redux";

export const App = () => {
  setConfiguration({
    maxScreenClass: "sm"
  })

  const auth = !!useSelector(state=>state.app.apiToken);

  return <Container>
    <Router>
      {Routes(auth)}
    </Router>
    <NotificationContainer />
  </Container>
}