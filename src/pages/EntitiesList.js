import {useDispatch} from "react-redux";
import {setCurrentPage, setNavMenuState, setPageTitle} from "../store/actions";
import React, {useEffect} from "react";
import {List} from "../components/list/List";
import {Pagination} from "../components/list/Pagination";

const types = {
    deal: "Сделки",
    lead: "Лиды",
    contact: "Контакты",
    company: "Компании"
}

export const EntitiesList = ({type}) => {
    const dispatch = useDispatch()

    useEffect(()=>{
        dispatch(setCurrentPage(0))
        dispatch(setNavMenuState(false))
        dispatch(setPageTitle(types[type]))
    },[dispatch, type])

    return <>
        <List type={type} />
        <Pagination />
    </>
}