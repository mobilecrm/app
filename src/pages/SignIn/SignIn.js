import React, {useReducer} from "react";
import styled from "styled-components";
import logo from "./logo.png"
import {Row, Col} from "react-grid-system";
import {Input} from "../../components/forms/inputs/Input";
import {Button} from "../../components/forms/buttons/Button";
import {auth} from "../../store/actions";
import {useDispatch} from "react-redux";


const Component = styled(Row)`
  img{
    margin: 19px auto;
  }
  
  h1{
    margin-top: 80px;
    margin-bottom: 30px;
    font-weight: 400;
    font-size: 20px;
    border-bottom: 1px solid #EEEEEE;
  }
`

export const SignIn = () => {

    const dispatch = useDispatch()

    const [formData, dispatchFormData] = useReducer((state, action)=>({...state, [action.name]: action.value}), {
        login: '',
        password: ''
    })

    return <Component>
        <img src={logo} alt={"Bitrix24 Logo"} />
        <Col xs={12}>
            <h1>Авторизация</h1>
        </Col>
        <Col xs={12}>
            <Input name="login" placeholder="Логин" onChange={e=>dispatchFormData(e.target)} />
        </Col>
        <Col xs={12}>
            <Input name="password" placeholder="Пароль" type="password" onChange={e=>dispatchFormData(e.target)} />
        </Col>
        <Col xs={4}/>
        <Col xs={4}>
            <Button
                rounded={1}
                onClick={()=>dispatch(auth(formData.login, formData.password))}
                color="primary">
                Войти</Button>
        </Col>
        <Col xs={4}/>
    </Component>
}